<?php
/*
 * Wolf CMS - Content Management Simplified. <http://www.wolfcms.org>
 * Copyright (C) 2012 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * This file is part of Wolf CMS. Wolf CMS is licensed under the GNU GPLv3 license.
 * Please see license.txt for the full license text.
 */

/* Security measure */
if (!defined('IN_CMS')) { exit(); }

/**
 * The wizard plugin provides wizard like functionality to other plugins as well
 * as site developers.
 *
 * @package Plugins
 * @subpackage wizard
 *
 * @author Martijn van der Kleijn <martijn.niji@gmail.com>
 * @copyright Martijn van der Kleijn, 2012
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 license
 */

/**
 * Handles all wizard steps.
 */
class WizardController extends PluginController {

    public function __construct() {
        if (CMS_BACKEND === true) {
            $this->setLayout('backend');
            $this->assignToLayout('sidebar', new View('../../plugins/wizard/views/sidebar'));            
        }
    }

    public function index() {
        $this->documentation();
    }

    public function documentation() {
        $this->display('wizard/views/documentation');
    }

    function settings() {
        $this->display('wizard/views/settings', Plugin::getAllSettings('wizard'));
    }
}