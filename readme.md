# Wizard plugin for Wolf CMS

## What it is

The wizard plugin makes functionality available to other plugins that allows them
to easily provide "wizard" functionality. In other words, walk through a set of
screens step-by-step.

## How to use it

Just:

* enable it,
* register a new wizard definition and
* point the user to the start of the wizard

## License information

Copyright 2012, Martijn van der Kleijn. <martijn.niji@gmail.com>
This plugin is licensed under the GPLv3 License.
<http://www.gnu.org/licenses/gpl.html>
