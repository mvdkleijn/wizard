<?php
/*
 * Wolf CMS - Content Management Simplified. <http://www.wolfcms.org>
 * Copyright (C) 2012 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * This file is part of Wolf CMS. Wolf CMS is licensed under the GNU GPLv3 license.
 * Please see license.txt for the full license text.
 */

/* Security measure */
if (!defined('IN_CMS')) { exit(); }

/**
 * The wizard plugin provides wizard like functionality to other plugins as well
 * as site developers.
 *
 * @package Plugins
 * @subpackage wizard
 *
 * @author Martijn van der Kleijn <martijn.niji@gmail.com>
 * @copyright Martijn van der Kleijn, 2012
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 license
 */

Plugin::setInfos(array(
    'id'          => 'wizard',
    'title'       => __('Wizard'),
    'description' => __('Provides wizard functionality.'),
    'version'     => '1.0.0',
    'license'     => 'GPL',
    'author'      => 'Martijn van der Kleijn',
    'website'     => 'http://www.wolfcms.org/',
    'update_url'  => 'http://www.wolfcms.org/plugin-versions.xml',
    'require_wolf_version' => '0.7.5-SP1'
));

AutoLoader::addFile('Spyc', PLUGINS_ROOT.'/wizard/lib/spyc.php');

Plugin::addController('wizard', __('Wizard'), 'admin_view', false);