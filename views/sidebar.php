<?php
/*
 * Wolf CMS - Content Management Simplified. <http://www.wolfcms.org>
 * Copyright (C) 2012 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * This file is part of Wolf CMS. Wolf CMS is licensed under the GNU GPLv3 license.
 * Please see license.txt for the full license text.
 */

/* Security measure */
if (!defined('IN_CMS')) { exit(); }

/**
 * The wizard plugin provides wizard like functionality to other plugins as well
 * as site developers.
 *
 * @package Plugins
 * @subpackage wizard
 *
 * @author Martijn van der Kleijn <martijn.niji@gmail.com>
 * @copyright Martijn van der Kleijn, 2012
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 license
 */
?>
<div class="box">
<h2><?php echo __('Wizard plugin');?></h2>
<p>
<?php echo __('Version: :version', array(':version' => '1.0.0'))?></br>
<?php echo __('Homepage: <a href=":url">:name</a>', array(':url' => 'http://www.vanderkleijn.net/software/wizard', ':name' => 'vanderkleijn.net'))?></br>
</p>
</div>
